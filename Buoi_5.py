# f(x) = x*x
# def func(x):
#   return x*x

# a = func(2)
# print(a)

# parameter: tham số vs argument: đồi số

# viết hàm nhập vào n số nguyên bất kỳ tìm số lớn nhất
# int float string bool list

# cho 1 danh sách số nguyên bất kỳ, viết ct sắp xếp theo thứ tự từ lớn đến bé và từ bé đến lớn các số đó.
def find_max(L):
  max = L[0]
  for i in range(len(L)):
    if L[i] > max:
      max = L[i]
  return max

def find_min(L):
  min = L[0]
  for i in range(len(L)):
    if L[i] < min:
      min = L[i]
  return min

def lon_be(L):
  re = []
  for i in range(len(L)):
    re.append(find_max(L))
    L.pop(L.index(find_max(L)))
  return re

def be_lon(L):
  re = []
  for i in range(len(L)):
    re.append(find_min(L))
    L.pop(L.index(find_min(L)))
  return re

L = [5,6,3,4,7,8,0,2,9]
print('lớn đến bé: ', lon_be(L))
L = [5,6,3,4,7,8,0,2,9]
print('bé đến lớn: ', be_lon(L))