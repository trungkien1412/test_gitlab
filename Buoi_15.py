# Bài tập 1: Viết ct tính chu vi và diện tích của một số hình sau: tròn, tam giác, vuông, chữ nhật, bình hành
# decorator

# def ham1(func):
#   print("bắt đầu hàm 2")
#   func('đang trong hàm 1')
#   print("kết thúc hàm 2")
#   return 

# @ham1
# def ham3(a):
#   print(" đây là hàm 3 ")
#   return a

# if __name__ == '__main__':
#   b = 'test'
#   ham3(b)
#   # print(a)

# import math
# class hinh:
#     dac_diem = """Là dạng thức của một vật thể hoặc bản phác thảo, đường biên, mặt phẳng ngoài của nó, 
#     đối lập với những thuộc tính khác như màu sắc, chất liệu hay thành phần vật liệu của vật thể đó. """
#     def __init__(self, so_canh, ten):
#         self.so_canh = so_canh
#         self.ten = ten

# class dagiac(hinh):
#     def __init__(self, so_canh, ten):
#         hinh.__init__(self, so_canh, ten)
    
# class tu_giac(dagiac):
#     def __init__(self, canh_a, canh_b, canh_c, canh_d, ten):
#         dagiac.__init__(self, 4, ten)
#         self.canh_a = canh_a
#         self.canh_b = canh_b
#         self.canh_c = canh_c
#         self.canh_d = canh_d
#     def chu_vi(self):
#         chuvi = self.canh_a + self.canh_b + self.canh_c + self.canh_d
#         return f"Chu vi là: {chuvi}"
# class tam_giac(dagiac):
#     def __init__(self, canh_a, canh_b, canh_c, ten):
#         dagiac.__init__(self, 3, ten)
#         self.canh_a = canh_a
#         self.canh_b = canh_b
#         self.canh_c = canh_c
#     def chu_vi(self):
#         chu_vi = self.canh_a + self.canh_b + self.canh_c
#         return f"Chu vi của tam giác là: {chu_vi}"
#     def dien_tich(self):
#         p = (self.canh_a + self.canh_b + self.canh_c) / 2
#         dien_tich = math.sqrt(p * (p - self.canh_a) * (p - self.canh_b) * (p - self.canh_c))
#         return f"Diện tích của tam giác là: {dien_tich}"

# class binh_hanh(tu_giac):
#     def __init__(self, canh_a, canh_b, duong_cao, ten):
#         tu_giac.__init__(self, canh_a, canh_b,canh_a, canh_b, ten)
#         self.dac_diem += "Là tứ giác có 2 cặp cạnh đối song song và bằng nhau. "
#         self.duong_cao = duong_cao
#     def dien_tich(self):
#         dien_tich = self.canh_a * self.duong_cao
#         return f"Diện tích là: {dien_tich}"

# class chu_nhat(binh_hanh):
#     def __init__(self, canh_a, canh_b, ten):
#         binh_hanh.__init__(self, canh_a, canh_b, canh_a, ten)
#         self.dac_diem += "Là hình bình hành có 1 góc vuông. "

# class hinh_vuong(chu_nhat):
#     def __init__(self, canh_a, ten):
#         chu_nhat.__init__(self, canh_a, canh_a, ten)
#         self.dac_diem += "Là hình chữ nhật có 2 cạnh liền bằng nhau. "

# class hinh_tron(hinh):
#     def __init__(self, bankinh, ten):
#         hinh.__init__(self, 0, ten)
#         self.bankinh = bankinh
#     def chu_vi(self):
#         chuvi = 2 * math.pi * self.bankinh
#         return f"Chu vi của hình tròn này là: {chuvi}"
#     def dien_tich(self):
#         dientich = math.pi * self.bankinh ** 2
#         return f"Diện tích của hình tròn này là: {dientich}"

# if __name__ == "__main__":
#     data = []
#     while True:
#         check = input("Bạn muốn nhập thông tin của hình gì (tròn:1, vuông:2, chữ nhật:3, bình hành:4, tam giác:5, in ra kết quả:6, thoát:0): ")
#         if check == '1':
#             bankinh = float(input("Nhập vào bán kính: "))
#             tron = hinh_tron(bankinh, "Hình Tròn")
#             data.append(tron)
#         elif check == '2':
#             canh = float(input("Nhập vào cạnh: "))
#             vuong = hinh_vuong(canh, "Hình Vuông")
#             data.append(vuong)
#         elif check == '3':
#             chieudai = float(input("Nhập vào chiều dài: "))
#             chieurong = float(input("Nhập vào chiều rộng: "))
#             chunhat = chu_nhat(chieudai,chieurong, "Hình Chữ Nhật")
#             data.append(chunhat)
#         elif check == '4':
#             day = float(input("Nhập vào đáy : "))
#             ben = float(input("Nhập vào cạnh bên thứ nhất: "))
#             cao = float(input("Nhập vào chiều cao: "))
#             binhhanh = binh_hanh(day,ben,cao, "Hình Bình Hành")
#             data.append(binhhanh)
#         elif check == '5':
#             canh1 = float(input("Nhập vào cạnh thứ nhất: "))
#             canh2 = float(input("Nhập vào cạnh thứ hai: "))
#             canh3 = float(input("Nhập vào cạnh thứ ba: "))
#             tamgiac = tam_giac(canh1,canh2,canh3,"Hình Tam giác")
#             data.append(tamgiac)
#         elif check == '0':
#             print("Thoát chương trình!!!!")
#             break
#         elif check == '6':
#             for item in data:
#                 print(f"-----------{item.ten}-----------")
#                 print(item.dac_diem)
#                 print(item.chu_vi())
#                 print(item.dien_tich())
#         else:
#             print("Bận đã nhập sai!!!!")
