# open(file, mode='r')
# mode: 'r': mở để đọc (mode mắc định)
#       'r+': mở để đọc và ghi
#       'w': mở để ghi. nếu file đã tồn tại trước đó thì file đó sẽ bị xóa hết nội dung và viết lại dữ liệu. 
#            Nếu file chưa tồn tại thì sẽ tạo ra file mới và ghi dữ liệu lên đó.
#       'w+': Mở để ghi và đọc. nếu file đã tồn tại trước đó thì file đó sẽ bị xóa hết nội dung và viết lại dữ liệu. 
#            Nếu file chưa tồn tại thì sẽ tạo ra file mới và ghi dữ liệu lên đó.
#       'a': mở để ghi. Nếu file chưa tồn tại thì sẽ tạo ra file mới và ghi dữ liệu lên đó.
#       'a+': Mở để ghi và đọc. Nếu file chưa tồn tại thì sẽ tạo ra file mới và ghi dữ liệu lên đó.

# bài 1: cho 1 file txt chứa 1 dãy các số tự nhiên
# viết ct kiểm tra chẵn lẻ và phân tách ra 2 file txt (1 file chứa các số chẵn, 1 file chứa các số lẻ)
# fi = open("test.TXT", mode = 'r')
# fo1 = open("ODD.OUT", mode = 'w')
# fo2 = open("EVEN.OUT", mode = 'w')
# input_string = fi.readline()
# lst = input_string.split(' ')
# for i in range(len(lst)):
#     lst[i] = int(lst[i])
#     if lst[i] % 2 == 0:
#         fo1.write(str(lst[i]) + " ")
#     else:
#         fo2.write(str(lst[i]) + " ")
# fi.close()
# fo1.close()
# fo2.close()
# import csv
# row = ['test', '11', '11082022test11@vaa.edu.vn', 'Abcd1234@']
# with open('users.csv','a') as f:
#   w = csv.writer(f)
#   w.writerow(row)
