# Bài tập: Viết ct tính lương cho nhân viên của một công ty. Thông tin của nhân viên trong công ty bao gồm: Họ tên, hệ số lương,
# số người giảm trừ gia cảnh.
# Nhân viên trong công ty được chia thành 2 loại:
#   * Nhân viên kinh doanh: mỗi nhân viên kinh doanh có thêm mức lương kinh doanh hằng tháng, được hưởng dựa trên doanh số đạt được
# trong tháng, tỷ lệ này từ 0% - 200%. Lương thưởng = lương cơ bản x tỷ lệ lương kinh doanh
#   * Nhân viên sản xuất: mỗi nhân viên sản xuất có 1 định mức sản phẩm làm ra hằng tháng là 450 sản phẩm, nếu số lượng sản phẩm vượt
# định mức thì sẽ được hưởng thêm theo công thức: lương thưởng = số sản phầm vượt định mức * đơn giá gia công sản phẩm. Nếu không vượt
# định mức thì lương thưởng = 0.

# - lương cơ bản = 1 260 000 VNĐ
# - đơn giá gia công sản phẩm = 10 000
###### lương của nhân viên được tính như sau:
# - thu nhập = hệ số lương x lương cơ bản + lương thưởng
# - thu nhập chịu thuế = thu nhập - 11 000 000 - 3 600 000 x số người giảm trừ gia cảnh
# - thuế tncn = dựa trên thu nhập chịu thuế và tính theo luật (đã học từ bài trước)
# - Thực lĩnh = thu nhập - thuế tncn
import Buoi_13 as b3

a = b3.sach('adsfasdf', 'Sach giao khoa', 'Kim đồng','ngày hôm nay')

print(a.ten_sach)
