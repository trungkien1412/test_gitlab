#Bài 1: 
# Với số nguyên n nhất định,
# hãy viết chương trình để tạo ra một dictionary chứa (i, i*i) như là số nguyên từ 1 đến n (bao gồm cả 1 và n) 
# sau đó in ra dictionary này. 
# Ví dụ: Giả sử số n là 8 thì đầu ra sẽ là: {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64}. 

#Bài 2:
# Viết chương trình tính số tiền thực của một tài khoản ngân hàng dựa trên nhật ký giao dịch được nhập vào từ giao diện điều khiển.  
# Định dạng nhật ký được hiển thị như sau: 
# D 100 
# W 200 
# (D là tiền gửi, W là tiền rút ra). 
# Giả sử đầu vào được cung cấp là: 
# D 300 
# D 300 
# W 200
# D 100 
# Thì đầu ra sẽ là: 500

#Bài 3: 
# Viết chương trình tính số tháng cần để đặt được mức tiền mong muốn khi gửi ngân hàng với lãi kép.
# T0 100 (10%/month) 130
# T1 110
# T2 121 120
# T3 133.1 130
def lai_kep(tien_gui, tien_nhan, ls):
    month = 0
    tong = tien_gui
    while tong < tien_nhan:
        tong = tong*(1+ls/100)# tong + tong*ls/100
        month += 1
    return month

tien_gui = int(input("Nhap so tien gui: "))
ls = int(input("Nhap lai suat (%/thang): "))
tien_nhan = int(input("Nhap so tien mong muon nhan lai: "))
th = lai_kep(tien_gui, tien_nhan, ls)
print("So thang de nhan duoc so tien mong muon: ", th, " thang")